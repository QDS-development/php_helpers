<?php

if(!function_exists('trimName')){
    function trimName($name){
        $exp = explode(' ', $name);
        return rtrim(ltrim($exp[0].' '.$exp[1], '('), ')');
    }
}

if(!function_exists('serviceLevel')){
    /**
     * @param $service_level_code
     * @param bool $database
     * @return string
     */
    function serviceLevel($service_level_code, $database = false){
        if($database){
            switch($service_level_code){
                case 0:  $price_type = ''           ; break; //recepciós
                case 1:  $price_type = 'standard'   ; break; //műkörmös senior
                case 2:  $price_type = 'standard'   ; break; //pedikűrös senior
                case 11: $price_type = 'gyakornok'  ; break; //műkörmös gyakornok
                case 12: $price_type = 'junior'     ; break; //műkörmös junior
                case 13: $price_type = 'top'        ; break; //műkörmös top
                case 21: $price_type = 'gyakornok'  ; break; //pedikűrös gyakornok
                case 22: $price_type = 'junior'     ; break; //pedikűrös junior
                case 23: $price_type = 'top'        ; break; //pedikűrös top
                default: $price_type = 'standard'   ; break;
            }
            return $price_type;
        }
        switch($service_level_code){
            case 0:  $price_type = ''           ; break; //recepciós
            case 1:  $price_type = 'Senior'     ; break; //műkörmös senior
            case 2:  $price_type = 'Senior'     ; break; //pedikűrös senior
            case 11: $price_type = 'Gyakornok'  ; break; //műkörmös gyakornok
            case 12: $price_type = 'Junior'     ; break; //műkörmös junior
            case 13: $price_type = 'TOP'        ; break; //műkörmös top
            case 21: $price_type = 'Gyakornok'  ; break; //pedikűrös gyakornok
            case 22: $price_type = 'Junior'     ; break; //pedikűrös junior
            case 23: $price_type = 'TOP'        ; break; //pedikűrös top
            default: $price_type = 'Senior'     ; break;
        }
        return $price_type;
    }
}

if(!function_exists('preferredTimes')){

    /**
     * @param array $times
     * @return array
     */
    function preferredTimes(array $times){
        if(empty($times)) return null;
        $return = [];
        for($hour = 8; $hour<=21; $hour++){
            if(isset($times[$hour])){
                $return[$hour] = $times[$hour];
            }else{
                end($return);
                $return[$hour] = isset($return[key($return)]) ? $return[key($return)] : 0;
            }
        }
        return $return;
    }
}

if(!function_exists('reOrderArray')){

    /**
     * @param array $array
     * @return array
     */
    function reOrderArray(array $array){
        $return = [];
        foreach($array as $v){
            $return[] = $v;
        }
        return $return;
    }
}



if(!function_exists('dd')){

    Kint::$aliases[] = 'dd';

    function dd(...$vars){
        Kint::dump(...$vars);
        exit;
    }

}

if(!function_exists('dxx')){

    
    function dxx(){

        $debug = debug_backtrace();
        foreach($debug as $backtrace){
            if($backtrace['function'] == 'dx'){
                $location = $backtrace['file'].' - '.$backtrace['line'];
                var_dump($location);
                break;
            }
        }
        foreach(func_get_args() as $arg){
            if(function_exists('xdebug_enable')){
                var_dump($arg);
            }else{
                echo '<pre style="margin-top: 25px; height: 70vh; overflow: scroll;">';
                echo var_export($arg, true);
                echo '</pre>';
            }

        }
    }

}

if(!function_exists('dx')){
    
    function dx(...$args){
        dxx(...$args);
        die();
    }
}

if(!function_exists('dda')){

    /**
     * @param null $item
     * @param null $live
     * @param bool $multiple
     */
    function dda($item = null, $live = null, $multiple = false){
        $debug = debug_backtrace();
        foreach($debug as $value){
            if($value['function'] == 'dda'){
                $location = $value['file'].' - '.$value['line'];
                echo $location;
                echo PHP_EOL;
                break;
            }
        }

        if($multiple && is_array($item)){
            foreach($item as $value) echo var_export($value);
        }else{
            echo var_export($item);
        }

        if(!$live) die();
    }
}

if(!function_exists('super_isset')){
    /**
     * @param $array
     * @param array $keys
     * @return bool
     */
    function super_isset($array, array $keys){
        $return = false;
        foreach($keys as $k => $key){
            if(isset($array[$key])){
                $return = true;
                if(1 < count($keys)){
                    unset($keys[$k]);
                    $return = super_isset($array[$key], $keys);
                }
                if(!$return) return false;
            }
        }
        return $return;
    }
}

if(!function_exists('ekezettelenit')){

    /**
     * @param $str
     * @return mixed|string
     */
    function ekezettelenit($str) {
        return nc($str);
    }
}

if(!function_exists('array_intersect_recursive')){

    /**
     * Két tömböt hasonlít össze, és csak azokat az elemeket
     * adja vissza, amelyek mindkét tőmbben szerepelnek.
     * Fontos, hogy a két tömbbnek ugyanolyan mélységűnek kell lennie!
     * @param $array1
     * @param $array2
     * @return mixed
     */
    function array_intersect_recursive($array1, $array2){
        foreach($array1 as $key => $value){
            if(!isset($array2[$key])){
                unset($array1[$key]);
            }else{
                if(is_array($array1[$key])){
                    $array1[$key] = array_intersect_recursive($array1[$key], $array2[$key]);
                }elseif($array2[$key] !== $value){
                    unset($array1[$key]);
                }
            }
        }
        return $array1;
    }
}

if(!function_exists('fetch_result_to_array')){

    /**
     * @param $result
     * @param string $keyID
     * @return array
     */
    function fetch_result_to_array($result, $keyID = 'id'){
        $return = [];
        while($asc = $result->fetch_assoc()){
            if($keyID && array_key_exists($keyID, $asc)){
                $return[ $asc[$keyID] ] = $asc;
            }else{
                $return[] = $asc;
            }
        }
        return $return;
    }
}

if(!function_exists('nextOpenDayOffset')){
    /**
     * @param string $date
     * @return int
     */
    function nextOpenDayOffset($date = ''){
        if($date){
            $date = strtotime($date);
        }else{
            $date = time();
        }

        $current_week_day_nr = date('w',$date);

        $next_open_day = 2;
        if($current_week_day_nr != 6){
            $next_open_day = 1;
        }
        return $next_open_day;
    }
}

if(!function_exists('urlSegment')){
    /**
     * Visszaadja a kívánt url szegmenst
     * @param $segment int
     * az url visszakapni kívánt szegmensének sorszáma
     * @return bool|string
     */
    function urlSegment($segment){
        $url = $_SERVER['REQUEST_URI'];
        $exp = explode('?', $url);
        $url = $exp[0];
        $aUrl = explode('/', $url);
        unset($aUrl[0]);

        if(array_key_exists($segment, $aUrl)){
            return $aUrl[$segment];
        }
        return false;
    }
}

if(!function_exists('isDateInAllowedInterval')){
    /**
     * Ellenőrzi, hogy a megadott dátum az engedélyezett
     * intervallumon belül van-e
     * @param $date
     * ellenőrizendő dátum
     * @param $intervalStart
     * napok a mai naphoz viszonyítva
     * @param $intervalEnd
     * napok a mai naphoz viszonyítva
     * @return bool
     */
    function isDateInAllowedInterval($date, $intervalStart, $intervalEnd){
        if(strtotime($date) < mktime(null, null, null, date('m'), date('d')+$intervalStart, date('y'))
            || strtotime($date) > mktime(null, null, null, date('m'), date('d')+$intervalEnd, date('y'))){
            return false;
        }else{
            return true;
        }
    }
}

if(!function_exists('mergeArray')){
    /**
     * Összefésül két tömböt úgy,
     * hogy az azonos kulcsok felülíródnak
     * a második tömbben
     * (az első tömb minden eleme megmarad)
     * @param $array1
     * @param $array2
     * @return mixed
     */
    function mergeArray($array1, $array2){
        foreach($array1 as $k => $v){
            $array2[$k] = $v;
        }
        return $array2;
    }
}

if(!function_exists('telszam')){
    /**
     * Telefonszám formátum készító és ellenőrző
     * @param $telszam
     * @param bool $href
     * @return bool|string
     */
    function telszam($telszam, $orszagkod = '36', $href = FALSE){
        $telszam = preg_replace('/[^0-9]/', '', $telszam); // előbb kihajigálom a nem számjegyeket
        preg_match('/^((?:[03]|003)6)?(?(?=([2357]0|1|31))([2357]0|1|31)(\d{7})|(2[2-9]|3[1-7]|4[024-9]|5[234679]|6[23689]|7[2-9]|8[02-9]|9[92-69])(\d{6}))$/', $telszam, $result);
        if($result){
            $result = array_unique($result);    // kiütöm a szükségtelen tömbelemeket
            $orszag = (empty($result[1]) || $result[1] == '06') ? $orszagkod : $result[1];  // Beállítom az országkódot, ha nincs megadva, 06 vagy 0036
            $orszag = ($orszag == 36 || $orszag == '0036') ? "+36" : $orszag;
            $szam1 = substr(end($result), 0, 3);
            $szam2 = substr(end($result), 3);  // 'egységes' alakra hozom a számot
            $korzet = prev($result);

            if(!$href){
                return $orszag . "(" . $korzet . ") " . $szam1 . "-" . $szam2;
            }else{
                return $orszag . $korzet . $szam1 . $szam2;
            }
            //Ha a telefonszám esetleg külföldi szám, akkor visszatérünk simán a számokkal
        }elseif(intval($telszam) && 10 < strlen($telszam)){
            return $telszam;
        }else{
            return FALSE;
        }
    }
}

if(!function_exists('getPropertyIfExist')){
    /**
     * Ha létezik a tulajdonság visszatér az értékével
     * @param $property
     * @param $_this
     * @return null
     */
    function getPropertyIfExist($property, $_this){
        if(property_exists($_this, $property)){
            return $_this->$property;
        }else{
            return null;
        }
    }
}

if(!function_exists('arrayToString')){
    /**
     * @param array $array
     * @param string $glue
     * @return string
     */
    function arrayToString(array $array, $glue = ' | '){

        $string = '';
        foreach($array as $k => $v){
            if(is_array($v)){
                $string .= $k.': ['.arrayToString($v, $glue).']'.$glue;
            }else{
                $string .= $k.': '.$v.$glue;
            }
        }
        return trim($string, $glue);
    }
}

if(!function_exists('trimServiceName')){
    /**
     * @param $serviceName
     * @return string
     */
    function trimServiceName($serviceName){
        $exp = explode(' - ', $serviceName);
        $serviceName = $exp[1];
        $exp = explode('(', $serviceName);
        return trim($exp[0]);
    }
}

if(!function_exists('nextWorkday')){
    /**
     * Visszaadja a következő munkanapot
     * @param $today
     * Az a nap, amihez képest a következő munkanapok visszakapjuk
     * @param array $workdays
     * Munkanapok számmal (1-7)
     * @return false|int|string
     * Ha unix a $today akkor unix, ha string akkor string
     */
    function nextWorkday($today, array $workdays){
        $weekdays = [1,2,3,4,5,6,7];
        $string = false;
        if(!in_array($today, $weekdays) && !isValidTimeStamp($today)){
            $today = strtotime($today);
            $string = true;
        }

        $next_workday = $today;
        $i = 0;
        do{
            $next_workday = strtotime("+1 day", $next_workday);
            $i++;
        }while(!in_array(date('N', $next_workday), $workdays) && $i < 100);

        if($string){
            $next_workday = date('Y-m-d', $next_workday);
        }

        return $next_workday;

    }
}

if(!function_exists('isValidTimeStamp')){
    /**
     * @param $timestamp
     * @return bool
     */
    function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp)
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }
}

if(!function_exists('intro')){
    /**
     * @param $string
     * @param int $maxLength
     * @param string $template
     * @return string
     */
    function intro($string, $maxLength = 150, $template = '%1$s'){
        $string = strip_tags($string); //formázás kivétele a szövegből
        //$format='/\A(.{0,'.$maxLength.'})[\s]\b/siu'; // [\s] miatt egy szóval nem működik

        $replace = array(
            ' ',
            '&',
            'Ö',
            'Ü',
            'Ó',
            'Ő',
            'Ú',
            'É',
            'Á',
            'Ű',
            'Í',
            'ö',
            'ü',
            'ó',
            'ő',
            'ú',
            'é',
            'á',
            'ű',
            'í',
            '"'
        );
        $search = array(
            '&nbsp;',
            '&amp;',
            '&Ouml;',
            '&Uuml;',
            '&Oacute;',
            '&Otilde;',
            '&Uacute;',
            '&Eacute;',
            '&Aacute;',
            '&Ucirc;',
            '&Iacute;',
            '&ouml;',
            '&uuml;',
            '&oacute;',
            '',
            '&uacute;',
            '&eacute;',
            '&aacute;',
            '&ucirc;',
            '&iacute;',
            '&quot;'
        );

        $OK_string = str_replace($search, $replace, $string);
        $format = '/\A(.{0,' . $maxLength . '})\b/siu'; //módosított reguláris kifejezés
        $result = '';
        $pm = preg_match($format, $OK_string, $result);
        $c = mb_strlen($OK_string);

        if($pm){
            return sprintf($template, rtrim($result[0])) . ($maxLength < $c ? ' ...' : '');
        }
        return '';
    }
}

if(!function_exists('array_get_value')){

    function array_get_value($needle, $haystack, &$result = null) {

        foreach($haystack as $key => $value) {
            if (is_array($value)) {
                array_get_value($needle, $value, $result);
            }
            else if($key === $needle) {
                $result = $value;
            }
            if($result) return $result;
        }
        return false;
    }
}

if(!function_exists('in_array_r')){

    /**
     * @param $needle
     * @param $haystack
     * @param bool $strict
     * @return bool
     */
    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }
}

if(!function_exists('clear_http_protocols')){

    /**
     * Eltávolítja a http:// és a https:// szövegrészt az url-ről
     * @param $url
     * @return string|string[]|null
     */
    function clear_http_protocols($url){

        return preg_replace('/(^\w+:|^)\/\//', '', $url);

    }
}

if(!function_exists('nc')){

    /**
     * @param $dat
     * @param false $seo
     * @param string $replacement
     * @return array|string
     */
    function nc($dat, $seo = false, $replacement = '-') {
        $convertTable = array(
            '&amp;' => 'and',   '@' => 'at',    '©' => 'c', '®' => 'r', 'À' => 'A',
            'Á' => 'A', 'Â' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'ae','Ç' => 'c',
            'È' => 'E', 'É' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'y',
            'ß' => 'ss','à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', 'å' => 'a',
            'æ' => 'ae','ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'þ' => 'p', 'ÿ' => 'y', 'Ā' => 'A',
            'ā' => 'a', 'Ă' => 'A', 'ă' => 'a', 'Ą' => 'A', 'ą' => 'a', 'Ć' => 'c',
            'ć' => 'c', 'Ĉ' => 'c', 'ĉ' => 'c', 'Ċ' => 'c', 'ċ' => 'c', 'Č' => 'c',
            'č' => 'c', 'Ď' => 'd', 'ď' => 'd', 'Đ' => 'd', 'đ' => 'd', 'Ē' => 'E',
            'ē' => 'e', 'Ĕ' => 'E', 'ĕ' => 'e', 'Ė' => 'E', 'ė' => 'e', 'Ę' => 'E',
            'ę' => 'e', 'Ě' => 'E', 'ě' => 'e', 'Ĝ' => 'g', 'ĝ' => 'g', 'Ğ' => 'g',
            'ğ' => 'g', 'Ġ' => 'g', 'ġ' => 'g', 'Ģ' => 'g', 'ģ' => 'g', 'Ĥ' => 'h',
            'ĥ' => 'h', 'Ħ' => 'h', 'ħ' => 'h', 'Ĩ' => 'I', 'ĩ' => 'I', 'Ī' => 'I',
            'ī' => 'i', 'Ĭ' => 'I', 'ĭ' => 'i', 'Į' => 'i', 'į' => 'i', 'İ' => 'i',
            'ı' => 'i', 'Ĳ' => 'ij','ĳ' => 'ij','Ĵ' => 'j', 'ĵ' => 'j', 'Ķ' => 'k',
            'ķ' => 'k', 'ĸ' => 'k', 'Ĺ' => 'l', 'ĺ' => 'l', 'Ļ' => 'l', 'ļ' => 'l',
            'Ľ' => 'l', 'ľ' => 'l', 'Ŀ' => 'l', 'ŀ' => 'l', 'Ł' => 'l', 'ł' => 'l',
            'Ń' => 'n', 'ń' => 'n', 'Ņ' => 'n', 'ņ' => 'n', 'Ň' => 'n', 'ň' => 'n',
            'ŉ' => 'n', 'Ŋ' => 'n', 'ŋ' => 'n', 'Ō' => 'o', 'ō' => 'o', 'Ŏ' => 'o',
            'ŏ' => 'o', 'Ő' => 'O', 'ő' => 'o', 'Œ' => 'oe','œ' => 'oe','Ŕ' => 'r',
            'ŕ' => 'r', 'Ŗ' => 'r', 'ŗ' => 'r', 'Ř' => 'r', 'ř' => 'r', 'Ś' => 's',
            'ś' => 's', 'Ŝ' => 's', 'ŝ' => 's', 'Ş' => 's', 'ş' => 's', 'Š' => 's',
            'š' => 's', 'Ţ' => 't', 'ţ' => 't', 'Ť' => 't', 'ť' => 't', 'Ŧ' => 't',
            'ŧ' => 't', 'Ũ' => 'U', 'ũ' => 'u', 'Ū' => 'U', 'ū' => 'u', 'Ŭ' => 'U',
            'ŭ' => 'u', 'Ů' => 'U', 'ů' => 'u', 'Ű' => 'U', 'ű' => 'u', 'Ų' => 'U',
            'ų' => 'u', 'Ŵ' => 'w', 'ŵ' => 'w', 'Ŷ' => 'y', 'ŷ' => 'y', 'Ÿ' => 'y',
            'Ź' => 'z', 'ź' => 'z', 'Ż' => 'z', 'ż' => 'z', 'Ž' => 'z', 'ž' => 'z',
            'ſ' => 'z', 'Ə' => 'e', 'ƒ' => 'f', 'Ơ' => 'O', 'ơ' => 'o', 'Ư' => 'U',
            'ư' => 'u', 'Ǎ' => 'A', 'ǎ' => 'a', 'Ǐ' => 'i', 'ǐ' => 'i', 'Ǒ' => 'O',
            'ǒ' => 'o', 'Ǔ' => 'u', 'ǔ' => 'u', 'Ǖ' => 'U', 'ǖ' => 'u', 'Ǘ' => 'U',
            'ǘ' => 'u', 'Ǚ' => 'u', 'ǚ' => 'u', 'Ǜ' => 'U', 'ǜ' => 'u', 'Ǻ' => 'A',
            'ǻ' => 'a', 'Ǽ' => 'ae','ǽ' => 'ae','Ǿ' => 'o', 'ǿ' => 'o', 'ə' => 'e',
            'Ё' => 'jo','Є' => 'E', 'І' => 'i', 'Ї' => 'i', 'А' => 'A', 'Б' => 'b',
            'В' => 'v', 'Г' => 'g', 'Д' => 'd', 'Е' => 'E', 'Ж' => 'zh','З' => 'z',
            'И' => 'i', 'Й' => 'j', 'К' => 'k', 'Л' => 'l', 'М' => 'm', 'Н' => 'n',
            'О' => 'O', 'П' => 'p', 'Р' => 'r', 'С' => 's', 'Т' => 't', 'У' => 'U',
            'Ф' => 'f', 'Х' => 'h', 'Ц' => 'c', 'Ч' => 'ch','Ш' => 'sh','Щ' => 'sch',
            'Ъ' => '-', 'Ы' => 'y', 'Ь' => '-', 'Э' => 'je','Ю' => 'ju','Я' => 'ja',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ж' => 'zh','з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh','щ' => 'sch','ъ' => '-','ы' => 'y', 'ь' => '-', 'э' => 'je',
            'ю' => 'ju','я' => 'ja','ё' => 'jo','є' => 'e', 'і' => 'i', 'ї' => 'i',
            'Ґ' => 'g', 'ґ' => 'g', 'א' => 'a', 'ב' => 'b', 'ג' => 'g', 'ד' => 'd',
            'ה' => 'h', 'ו' => 'v', 'ז' => 'z', 'ח' => 'h', 'ט' => 't', 'י' => 'i',
            'ך' => 'k', 'כ' => 'k', 'ל' => 'l', 'ם' => 'm', 'מ' => 'm', 'ן' => 'n',
            'נ' => 'n', 'ס' => 's', 'ע' => 'e', 'ף' => 'p', 'פ' => 'p', 'ץ' => 'C',
            'צ' => 'c', 'ק' => 'q', 'ר' => 'r', 'ש' => 'w', 'ת' => 't', '™' => 'tm',
            '&nbsp;' => '-', ',' => '-', '/' => '-', '\\' => '-', ' ' => '-',
            '%' => '-', '[' => '', ']' => '','(' => '', ')' => '', ':' => '-'
        );

        if (is_string($dat)) {
            if($seo) {
                $tmp = strtr($dat, $convertTable);
                $t2 = preg_replace('/[^0-9a-zA-Z:,]+/', $replacement, $tmp);
                $final = preg_replace('/(-{2,}|"|\')/', $replacement, $t2);
                return strtolower(trim($final));
            }else{
                $tmp = strtr($dat, $convertTable);
                $final = preg_replace('/(-{2,}|"|\')/', $replacement, $tmp);
                return strtolower(trim($final));
            }
            return $final;
        } elseif (is_array($dat)) {
            $ret = [];
            foreach ($dat as $i => $d) $ret[ $i ] = nc($d, $seo, $replacement);

            return $ret;
        } elseif (is_object($dat)) {
            foreach ($dat as $i => $d) $dat->$i = nc($d, $seo, $replacement);

            return $dat;
        } else {
            return $dat;
        }

    }
}

if(!function_exists('roundToTheNearestAnything')){

    /**
     * A kapott értékhez kerekíti a beadott integert
     *
     * @param $value
     * @param $roundTo
     * @return int
     */
    function roundToTheNearestAnything($value, $roundTo){
        $mod = $value % $roundTo;
        return $value + ($mod < ($roundTo / 2) ? -$mod : $roundTo - $mod);
    }
}



if(!function_exists('active')){

    /**
     * Visszaadja az aktív osztályt ha a $a == $b
     *
     * @param $a
     * @param $b
     * @param string $class
     * @return string
     */
    function active($a, $b, $class = 'active') {
        if($a == $b) 
            return $class;
    }
}